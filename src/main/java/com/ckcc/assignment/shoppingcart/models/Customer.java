package com.ckcc.assignment.shoppingcart.models;

import javax.persistence.*;

@Entity
@Table(name="tb_customer")
public class Customer {
	
	@Id
	private int id;
	private int identify;
	private String name;
	private String email;
	private String phone;
	private String shippingAddress;
	private String billingAddress;
	
	@OneToOne(mappedBy="customer")
	private Order order;
	
	@Transient
	private Cart shoppingCart;
	
	public Customer() {}
	public Customer(int identify, String name, String email, String phone, String shippingAddress, String billingAddress) {
		super();
		this.identify = identify;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.shippingAddress = shippingAddress;
		this.billingAddress = billingAddress;
	}
	
	public void placeOrder(Cart shoppingCart) {
		this.shoppingCart = shoppingCart;
	}
	
	public void cancelOrder() {
		this.shoppingCart = null;
	}
	
	public int getID() {
		return identify;
	}
	
	public Cart getShoppingCart() {
		return shoppingCart;
	}
	
	public String checkOut() {
		String shoppingCartOutput = "";
		shoppingCartOutput += "Sub Total : " + this.shoppingCart.calculateSubTotal() + "\n";
		shoppingCartOutput += "Discount : " + (this.shoppingCart.getDiscount() * 100) + "%" + "\n";
		shoppingCartOutput += "Total : " + String.format("%.2f",this.shoppingCart.calculateTotal());
		return shoppingCartOutput;
	}
	
	@Override
	public String toString() {
		String customerInfo = "";
		customerInfo += "[ ID\t: " + this.id + " ]" + "\n";
		customerInfo += "[ Name\t: " + this.name + " ]" + "\n";
		customerInfo += "[ Email\t: " + this.email + " ]" + "\n";
		customerInfo += "[ Phone\t: " + this.phone + " ]" + "\n";
		customerInfo += "[ Ship To\t: " + this.shippingAddress + " ]" + "\n";
		customerInfo += "[ Bill To\t: " + this.billingAddress + " ]";
		return customerInfo;
	}
	
}
