package com.ckcc.assignment.shoppingcart.models;

import javax.persistence.*;

@Entity
@Table(name="tb_order_detail")
public class OrderDetail {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private int productID;
	private double qty;
	private double discount;
	private double subtotal;
	private double price;
	
	public OrderDetail() {}
	public OrderDetail(int pID, double qty, double discount, double subtotal, double price) {
		super();
		this.productID = pID;
		this.qty = qty;
		this.discount = discount;
		this.subtotal = subtotal;
		this.price = price;
	}
	
}
