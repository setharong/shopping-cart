package com.ckcc.assignment.shoppingcart.models;

import java.util.*;

public class Cart {
	
	private List<Purchase> purchase;
	private double discount;
	
	public Cart() {
		super();
		this.purchase = new ArrayList<Purchase>();
	}
	
	public void addItem(Purchase item) {
		purchase.add(item);
	}
	
	public void removeItem(Purchase item) {
		purchase.remove(item);
	}
	
	public List<Purchase> getPurchasedItems() {
		return purchase;
	}
	
	public void clearPurchasedItem() {
		purchase = new ArrayList<Purchase>();
	}
	
	public double getDiscount() {
		return discount;
	}
	
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
	public double calculateSubTotal() {
		double subTotal = 0;
		for(Purchase pItem : purchase) {
			subTotal += pItem.getPrice();
		}
		return subTotal;
	}
	
	public double calculateTotal() {
		return calculateSubTotal() * (1 - discount);
	}
	
}
