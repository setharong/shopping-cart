package com.ckcc.assignment.shoppingcart.models;

import javax.persistence.*;

@Entity
@Table(name="tb_product")
public class Product {
	
	@Id
	private int id;
	private String code;
	private String name;
	private String description;
	private double price;
	private String brand;
	private String productOf;
	private int qtyInStock;
	
	public Product() {}
	public Product(int id, String code, String name, String description, double price, String brand, String productOf, int qtyInStock) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.description = description;
		this.price = price;
		this.brand = brand;
		this.productOf = productOf;
		this.qtyInStock = qtyInStock;
	}
	
	public boolean isValidStock(double qty) {
		return (qty > qtyInStock)? false: true;
	}
	
	public int getID() {
		return id;
	}
	
	public String getCode() {
		return code;
	}
	
	public String getName() {
		return name;
	}
	
	public double getPrice() {
		return price;
	}
	
	public double getQtyInStock() {
		return qtyInStock;
	}
	
	public Object[] getDataModel() {
		return new Object[] { this.id, this.code, this.name, this.description, this.price, this.brand, this.productOf, this.qtyInStock};
	}
	
}
