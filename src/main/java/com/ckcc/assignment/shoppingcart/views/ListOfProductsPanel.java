package com.ckcc.assignment.shoppingcart.views;

import java.awt.BorderLayout;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import com.ckcc.assignment.shoppingcart.controllers.DBController;
import com.ckcc.assignment.shoppingcart.models.Product;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ListOfProductsPanel extends MyPanel {
	
	private JTable table;
	private JButton btnBack;

	/**
	 * Create the panel.
	 */
	public ListOfProductsPanel(final MainForm parent) {
		
		table = new JTable();
		table.setRowSelectionAllowed(false);
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
			},
			new String[] {
				"ID", "Code", "Name", "Description", "Price", "Brand", "Product Of", "Quantity"
			}
		));
		table.setFocusable(false);
		setLayout(new BorderLayout(0, 0));
		add(table.getTableHeader(), BorderLayout.NORTH);
		add(table, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setHgap(20);
		add(panel, BorderLayout.SOUTH);
		
		JLabel lblNewLabel = new JLabel("Any details.......");
		panel.add(lblNewLabel);
		
		btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				parent.returnView();
			}
		});
		panel.add(btnBack);

		insertItemToTable();
		
	}
	
	private void insertItemToTable() {
		
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.setRowCount(0);
		for(Product p : DBController.getProductList()) {
			model.addRow(p.getDataModel());
		}
	}

	public void init() {
		// TODO Auto-generated method stub
		btnBack.requestFocus();
	}
	
}
