package com.ckcc.assignment.shoppingcart.views;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;

public class MainForm extends JFrame {

	/**
	 * Create the frame.
	 */
	private JPanel panelViewMid;
	
	public MainForm() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1280, 720);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panelHead = new JPanel();
		contentPane.add(panelHead, BorderLayout.NORTH);
		panelHead.setLayout(new BorderLayout(0, 0));
		
		JPanel panelTitle = new JPanel();
		panelHead.add(panelTitle, BorderLayout.NORTH);
		
		JLabel lblTitle = new JLabel("Shopping Cart");
		panelTitle.add(lblTitle);
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		JSeparator separator = new JSeparator();
		panelHead.add(separator);
		
		JPanel panelBody = new JPanel();
		contentPane.add(panelBody, BorderLayout.CENTER);
		panelBody.setLayout(new BorderLayout(0, 0));
		
		JPanel panelMenu = new JPanel();
		panelBody.add(panelMenu, BorderLayout.NORTH);
		panelMenu.setLayout(new BorderLayout(0, 0));
		
		JPanel panelSelect = new JPanel();
		panelMenu.add(panelSelect, BorderLayout.NORTH);
		
		JLabel lblMainMenu = new JLabel("Main Menu : ");
		panelSelect.add(lblMainMenu);
		
		comboBox = new JComboBox();
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent ie) {
				refreshView();
			}
		});
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"0. Home.", "1. New Product.", "2. List of Products.", "3. Go to Shopping.", "4. My Shopping Cart.", "5. Check Out."}));
		panelSelect.add(comboBox);
		
		JSeparator separator_2 = new JSeparator();
		panelMenu.add(separator_2, BorderLayout.CENTER);
		
		JPanel panelView = new JPanel();
		panelBody.add(panelView, BorderLayout.CENTER);
		panelView.setLayout(new BorderLayout(0, 0));
		
		JPanel panelUp = new JPanel();
		panelView.add(panelUp, BorderLayout.NORTH);
		panelUp.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblNewLabel = new JLabel("");
		panelUp.add(lblNewLabel);
		
		JPanel panel = new JPanel();
		panelUp.add(panel);
		
		lblTitle2 = new JLabel("Title");
		panel.add(lblTitle2);
		lblTitle2.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		JLabel label = new JLabel("");
		panelUp.add(label);
		
		panelViewMid = new JPanel();
		panelViewMid.setLayout(new BorderLayout());
		panelView.add(panelViewMid, BorderLayout.CENTER);
		
		JPanel panelBottom = new JPanel();
		panelView.add(panelBottom, BorderLayout.SOUTH);
		panelBottom.setLayout(new GridLayout(0, 1, 0, 0));
		refreshView();
		
		JPanel panelTail = new JPanel();
		contentPane.add(panelTail, BorderLayout.SOUTH);
		panelTail.setLayout(new BorderLayout(0, 0));
		
		JSeparator separator_1 = new JSeparator();
		panelTail.add(separator_1, BorderLayout.NORTH);
		
		JPanel panelMessage = new JPanel();
		panelTail.add(panelMessage, BorderLayout.SOUTH);
		
		JLabel lblMessage = new JLabel("Hello World.");
		panelMessage.add(lblMessage);
	}
	
	private JComboBox comboBox;
	private JLabel lblTitle2;
	
	private void refreshView() {
		int itemIndex = comboBox.getSelectedIndex();
		MyPanel panel = (itemIndex == 1)? new NewProductPanel(this): (itemIndex == 2)? new ListOfProductsPanel(this): (itemIndex == 3)? new GoToShoppingPanel(this): 
						(itemIndex == 4)? new MyShoppingCartPanel(this): (itemIndex == 5)?  new CheckOutPanel(this): new HomePagePanel();
		panelViewMid.removeAll();
		lblTitle2.setText(comboBox.getSelectedItem().toString());
		panelViewMid.add((Component) panel, BorderLayout.CENTER);

		panel.addListionerForComponents(panel);
		panel.init();

		revalidate();
		repaint();
	}

	public void returnView() {
		// TODO Auto-generated method stub
		comboBox.setSelectedIndex(0);
		refreshView();
	}
	
}
