package com.ckcc.assignment.shoppingcart.views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import com.ckcc.assignment.shoppingcart.controllers.DBController;
import com.ckcc.assignment.shoppingcart.models.*;

public class NewProductPanel extends MyPanel {
	
	private JTextField txtProductName;
	private JTextField txtProductDescription;
	private JTextField txtProductPrice;
	private JTextField txtProductQty;
	private JTextField txtProductID;
	private JTextField txtProductCode;
	private JTextField txtProductBrand;
	private JTextField txtProductOf;

	/**
	 * Create the panel.
	 */
	public NewProductPanel(final MainForm parent) {
		
		setLayout(new GridLayout(0, 2, 5, 5));
		
		JLabel lblNewLabel = new JLabel("Product ID :");
		add(lblNewLabel);
		
		txtProductID = new JTextField();
		add(txtProductID);
		txtProductID.setColumns(10);
		
		JLabel lblProductCode = new JLabel("Product Code :");
		add(lblProductCode);
		
		txtProductCode = new JTextField();
		add(txtProductCode);
		txtProductCode.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Product Name :");
		add(lblNewLabel_1);
		
		txtProductName = new JTextField();
		add(txtProductName);
		txtProductName.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Product Description :");
		add(lblNewLabel_2);
		
		txtProductDescription = new JTextField();
		add(txtProductDescription);
		txtProductDescription.setColumns(10);
		
		JLabel lblProductPrice = new JLabel("Product Price :");
		add(lblProductPrice);
		
		txtProductPrice = new JTextField();
		add(txtProductPrice);
		txtProductPrice.setColumns(10);
		
		JLabel lblProductBrand = new JLabel("Product Brand :");
		add(lblProductBrand);
		
		txtProductBrand = new JTextField();
		add(txtProductBrand);
		txtProductBrand.setColumns(10);
		
		JLabel lblProductOf = new JLabel("Product Of :");
		add(lblProductOf);
		
		txtProductOf = new JTextField();
		add(txtProductOf);
		txtProductOf.setColumns(10);
		
		JLabel lblProductQuantity = new JLabel("Product Quantity :");
		add(lblProductQuantity);
		
		txtProductQty = new JTextField();
		add(txtProductQty);
		txtProductQty.setColumns(10);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSaveEvent();
			}
		});
		add(btnSave);
		
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				parent.returnView();
			}
		});
		add(btnClose);
		
	}
	
	private void btnSaveEvent() {
		String title = "", message = "";
		//get data
		try {
			int id = Integer.parseInt(txtProductID.getText());
			String code = txtProductCode.getText();
			String name = txtProductName.getText();
			String description = txtProductDescription.getText();
			double price = Double.parseDouble(txtProductPrice.getText());
			String brand = txtProductBrand.getText();
			String productOf = txtProductOf.getText();
			int qtyInStock = Integer.parseInt(txtProductQty.getText());
			//insert object to controller
			boolean isCompleted = DBController.addProduct(new Product(id, code, name, description, price, brand, productOf, qtyInStock));
			if(isCompleted) {
				title = "Inserted.";
				message = "the product " + id + ", " + name + "has been inserted to database.";
				clear();
			}else {
				title = "Failed.";
				message = "the product with id= " + id + " already exist in database.";
				init();
			}
		}catch (NumberFormatException e) {
			// TODO: handle exception
			title = "Incorrected Input.";
			message = "Please input only number at fields ID, Price and Qty.";
		}finally {
			JOptionPane.showMessageDialog(this, message, title, JOptionPane.PLAIN_MESSAGE);
		}
	}
	
	private void clear() {
		txtProductID.setText("");
		txtProductCode.setText("");
		txtProductName.setText("");
		txtProductDescription.setText("");
		txtProductPrice.setText("");
		txtProductBrand.setText("");
		txtProductOf.setText("");
		txtProductQty.setText("");
	}

	public void init() {
		// TODO Auto-generated method stub
		txtProductID.requestFocus();
	}
	
}
