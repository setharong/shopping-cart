package com.ckcc.assignment.shoppingcart.views;

import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JTextArea;

import com.ckcc.assignment.shoppingcart.models.Customer;
import javax.swing.JSeparator;

public class CheckOutDetailPanel extends MyPanel {
	
	private JTextArea textAreaPurchaseInvoice;
	private JTextArea textAreaCheckOut;

	/**
	 * Create the panel.
	 */
	public CheckOutDetailPanel() {
		setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panel = new JPanel();
		add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.NORTH);
		
		JLabel label = new JLabel("Your Purchase Invoice :");
		panel_1.add(label);
		
		JPanel panel_2 = new JPanel();
		panel.add(panel_2);
		panel_2.setLayout(new GridLayout(0, 1, 0, 0));
		
		textAreaPurchaseInvoice = new JTextArea();
		panel_2.add(textAreaPurchaseInvoice);
		
		JPanel panel_3 = new JPanel();
		add(panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_5 = new JPanel();
		panel_3.add(panel_5, BorderLayout.NORTH);
		
		JLabel label_9 = new JLabel("Product Detail");
		panel_5.add(label_9);
		
		ListOfPurchasesPanel plPanel = new ListOfPurchasesPanel();
		panel_3.add(plPanel, BorderLayout.CENTER);
		
		JPanel panel_4 = new JPanel();
		panel_3.add(panel_4, BorderLayout.SOUTH);
		panel_4.setLayout(new BorderLayout(0, 0));
		
		JSeparator separator = new JSeparator();
		panel_4.add(separator, BorderLayout.NORTH);
		
		textAreaCheckOut = new JTextArea();
		textAreaCheckOut.setRows(3);
		panel_4.add(textAreaCheckOut);
		
	}
	
	public void showData(Customer customer) {
		textAreaPurchaseInvoice.setText(customer.toString());
		textAreaCheckOut.setText(customer.checkOut());
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

}
