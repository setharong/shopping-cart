package com.ckcc.assignment.shoppingcart.views;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.BorderLayout;

import com.ckcc.assignment.shoppingcart.controllers.DBController;
import com.ckcc.assignment.shoppingcart.models.Purchase;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MyShoppingCartPanel extends MyPanel {
	
	private JTable table;
	private JButton btnBack;

	/**
	 * Create the panel.
	 */
	public MyShoppingCartPanel(final MainForm parent) {
		setLayout(new BorderLayout(0, 0));
		
		add(new ListOfPurchasesPanel(), BorderLayout.CENTER);
		
		JPanel panelBottom = new JPanel();
		add(panelBottom, BorderLayout.SOUTH);
		
		JLabel label = new JLabel("Any details.......");
		panelBottom.add(label);
		
		btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parent.returnView();
			}
		});
		panelBottom.add(btnBack);
		
	}

	public void init() {
		// TODO Auto-generated method stub
		btnBack.requestFocus();
	}
	
}
