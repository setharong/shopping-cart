package com.ckcc.assignment.shoppingcart.views;

import javax.swing.*;
import java.awt.*;

public class HomePagePanel extends MyPanel {

	/**
	 * Create the panel.
	 */
	public HomePagePanel() {
		
		setLayout(new GridLayout(0, 1, 5, 5));
	
		JLabel lblPic1 = new JLabel("");
		lblPic1.setHorizontalAlignment(SwingConstants.CENTER);
		ImageIcon icon1 = new ImageIcon("resources/icon.jpg");
		lblPic1.setIcon(icon1);
		add(lblPic1, BorderLayout.NORTH);
	}

	public void init() {
		// TODO Auto-generated method stub
		
	}

}
